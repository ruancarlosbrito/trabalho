package principal;

import javax.swing.JOptionPane;

public class Veiculo {
    
    float consumo;
    float qtdCombustivel;

    public Veiculo(float fConsumo) {
        this.consumo = fConsumo;
        this.qtdCombustivel = 0;
    }
    
    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }

    public float getQtdCombustivel() {
        return qtdCombustivel;
    }

    public void setQtdCombustivel(float qtdCombustivel) {
        this.qtdCombustivel = qtdCombustivel;
    }

    public void obterGasolina(){
        JOptionPane.showMessageDialog(null, this.getQtdCombustivel());
    }
    
    public void gastarCombustivel(float fCombutivelGasto) {
        this.setQtdCombustivel(this.getQtdCombustivel() - fCombutivelGasto);
    }
    
    public void andar(float fDistancia){
        float fCombutivelGasto = fDistancia/(float)this.getConsumo();
        this.gastarCombustivel(fCombutivelGasto);
    }
    
    public void adicionaGasolina(float fQtdGasolina){
        this.setQtdCombustivel(this.getQtdCombustivel() + fQtdGasolina);
    }
}