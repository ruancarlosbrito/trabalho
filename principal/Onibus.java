package principal;

import javax.swing.JOptionPane;

public class Onibus extends Veiculo {
    
    int capacidade;
    int ocupacao;
    
    public Onibus(float fConsumo) {
        super(fConsumo);
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getOcupacao() {
        return ocupacao;
    }

    public void setOcupacao(int ocupacao) {
        this.ocupacao = ocupacao;
    }

    public void entrar(){
        this.setOcupacao(this.getOcupacao() + 1);
    }

    public void sair(){
        this.setOcupacao(this.getOcupacao() - 1);
    }

    public void mostrarOcupacao(){
        JOptionPane.showMessageDialog(null, this.getOcupacao());
    }
}