package trabalho;

class Acomodacao {

    private int numero;
    private int maximoPessoas;
    private float tamanho;
    private int vagas;
    private boolean livre;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getMaximoPessoas() {
        return maximoPessoas;
    }

    public void setMaximoPessoas(int maximoPessoas) {
        this.maximoPessoas = maximoPessoas;
    }

    public float getTamanho() {
        return tamanho;
    }

    public void setTamanho(float tamanho) {
        this.tamanho = tamanho;
    }

    public int getVagas() {
        return vagas;
    }

    public void setVagas(int vagas) {
        this.vagas = vagas;
    }

    public boolean isLivre() {
        return livre;
    }

    public void setLivre(boolean livre) {
        this.livre = livre;
    }
}